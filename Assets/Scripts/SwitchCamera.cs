﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamera : MonoBehaviour {

    public GameObject cameraOne;
    public GameObject cameraTwo;
  

    AudioListener audioListenerOne;
    AudioListener audioListenerTwo;

	// Use this for initialization
	void Start () {
       
        //get camera listener
        audioListenerOne = cameraOne.GetComponent<AudioListener>();
        audioListenerTwo = cameraTwo.GetComponent<AudioListener>();

        //setting camera position
        cameraPositionChange(PlayerPrefs.GetInt("CameraPosition"));

    }

  
	
	// Update is called once per frame
	void Update () {
        switchCamera();
	}
     
    //UI Joystick
     void cameraPositionM()
    {
        cameraChangeCounter();
    }

     void switchCamera()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            cameraChangeCounter();
        }
    }

    // Camera Counter
    void cameraChangeCounter()
    {
        int cameraPositionCounter = PlayerPrefs.GetInt("CameraPosition");
        cameraPositionCounter++;
        cameraPositionChange(cameraPositionCounter);
    }

    void cameraPositionChange(int camPosition)
    {
        if (camPosition > 1)
        {
            camPosition = 0;
        }

        // set camera position  database
        PlayerPrefs.SetInt("CameraPosition", camPosition);
        //set camera.position 1
        if (camPosition == 0)
        {
            cameraOne.SetActive(true);
            audioListenerOne.enabled = true;

            cameraTwo.SetActive(false);
            audioListenerTwo.enabled = false;
        }

        // set camera position 2 
        if (camPosition ==  1)
        {
            cameraTwo.SetActive(true);
            audioListenerTwo.enabled = true;

            cameraOne.SetActive(true);
            audioListenerOne.enabled = true;
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

    public static int live;
    public static int money;
    [SerializeField] private Animator hearthAnimator;
    [SerializeField] private Animator hearthAnimator2;
    [SerializeField] private Animator hearthAnimator3;
    [SerializeField] private Animator hearthAnimator4;
    [SerializeField] private Animator hearthAnimator5;
    [SerializeField] private Animator hearthAnimator6;
    public int startMoney = 1000;
    public int startLive = 6;
    [SerializeField] private Image[] hearths;
    public Text moneyText;
    Bullet bullet;

    // Use this for initialization
    void Start () {
        money = startMoney;
        live = startLive;
    }
	
	// Update is called once per frame
	void Update ()
	{
		if (hearths.Length > live)
		{
			hearths[live].enabled = false;
		}

		isLowLive(); // triggert später animation
		moneyText.text = "Money: " + money + "$";
	}

	void isLowLive()
	{
		if (live >= 1 && live <= 3)
		{
			hearthAnimator.SetBool("lowLive", true);
			hearthAnimator2.SetBool("lowLive", true);
			hearthAnimator3.SetBool("lowLive", true);
			hearthAnimator4.SetBool("lowLive", true);
			hearthAnimator5.SetBool("lowLive", true);
			hearthAnimator6.SetBool("lowLive", true);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageLive : MonoBehaviour {
    public static bool gameOver = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if( gameOver)
        {
            return;
        }
        if ( PlayerStats.live <= 0)
        {
            EndGame();
        }
	}

    private void EndGame()
    {
        gameOver = true;
        Debug.Log("Game Over!"); 
       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMovement : MonoBehaviour {

    [Header("Enemy Options")]
    public float startSpeed = 70f;
    [HideInInspector]
    public float speed;
    public float startHealth = 100;
    public float health;
    public int incrementMoney = 50;
    public Animator enemyAnimator;

    [Header("unity specific")] [SerializeField]
    private Image healthBar;
    
    private Transform target;
    private int wavepointIndex = 0;
    private bool dead = false;
    
    [Header("Offset Y prefab")]
    public  float offsetY = -4;

    public Vector3 offset;
    // Refferenz auf das Waypoints array an stelle 0 erster WP
    private void Start()
    {
        speed = startSpeed;
        health = startHealth;
        target = Waypoints.waypoints[0];
    }
   

    public void TakeDMG(float amount)
    {
        health -= amount;
        healthBar.fillAmount = health / startHealth;
          
        if( health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        PlayerStats.money += incrementMoney;

        dead = true;

        if (dead == true)
        {
            enemyAnimator.SetBool("dead", true);
        }


        Example();

        Debug.Log("Enemy Died");
    }

    IEnumerator Example()
    {
        if (health <= 0)
        {
            yield return new WaitForSeconds(2.2f); //this will wait 2.2 seconds (dying animation time)
            WaveSpawner.EnemiesAlive--;
            Destroy(gameObject);
        }
    }
        public void Slow (float percent)
        {
            speed = startSpeed * (1f - percent);
            speed = Mathf.Clamp(speed, 1f, 100f);
            print(speed +" -- >speed");
    }

    // jedes Frame aufgerufen, wir wollen etwas näher zum Ziel bewegen
    private void Update()
    {
        // stop moving when dead true
        if (dead)
        {
            speed = 0;
        }

        StartCoroutine(Example()); // start Timer
        Vector3 direction = (target.position - transform.position) - offset;
        transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);
        // Rotation in Wegpunkt 
        Quaternion targetRotation =  Quaternion.LookRotation(direction);
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 5);
        // wenn wir den Wegpunkt erreichen bzw. indem Fall kurz davor sind. 0.2
        // dann rufe naechsten Wegpunkt auf
        if ( Vector3.Distance(transform.position, target.position - offset)  <= 0.8f)
        {
            GetNextWaypoint();
        }
        speed = startSpeed;
    }
    void GetNextWaypoint()
    {
        if( wavepointIndex >= Waypoints.waypoints.Length - 1)
        {
            Destroy(gameObject);
           
            EndPath();
            return;
        }
        wavepointIndex++;
        target = Waypoints.waypoints[wavepointIndex];
    }

    void EndPath()
    {// Nur übergangsweise damit live nicht negativ wird
        if(PlayerStats.live > 0)
        {
            PlayerStats.live--;
        }
        WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
    }

    public float getHealth()
    {
        return this.health;
    }
}

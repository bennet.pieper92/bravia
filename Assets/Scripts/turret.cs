﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turret : MonoBehaviour {

    public Transform target;
    private EnemyMovement Enemy;
    [Header("General")]

    [Header("Use Bullets (default)")]
    public float fireRate = 1f;
    private float fireCountdown = 0f;
    public GameObject bulletPrefab;
    public Transform firePoint;

    [Header("Unity Setup Fields")]
    public string enemyTag = "Enemy";
   
    [Header("Use Laser")]
    public int damageOverTime = 25;
    public float slowPercent = .5f;
    public bool useLaser = false;
    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
 
    // reference to part to rotate
    [Header("Turret Settings")]
    public Transform partToRotate;
    public float turnSpeed = 10f;
    public float range = 15f;

    [Header("Sounds")] 
    public AudioSource shootSound;
    public bool isLoop;
    // Use this for initialization
    void Start () {
        InvokeRepeating("UpdateTarget", 0F, 0.5f);
	}

    // don't spend to much computing power and calculate every 2 Sec instead every Frame! 
    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        // default inifinity well when we found no enemy we have a infinite distance to that enemy.
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach(GameObject enemy in enemies)
        {
           
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            // when we find any enemy closer than the shortest distance to enemy before
            if (distanceToEnemy < shortestDistance && enemy.GetComponent<EnemyMovement>().health > 0)
            {
                // set new shortest enemy
                shortestDistance = distanceToEnemy;
                // tell the computer the shortest Enemy is this one
                nearestEnemy = enemy;
            }
        }
        if (nearestEnemy != null && shortestDistance <=range)
        {
        
            target = nearestEnemy.transform;
            Enemy = nearestEnemy.GetComponent<EnemyMovement>();
            
        }
        else
        {
            target = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // don't anything while target equals null, because we do everythink with the target
        if (target == null)
        {
            if (useLaser)
            {
                lineRenderer.enabled = false;
                shootSound.Stop();
                impactEffect.Stop();
            }
            return;
        }
        
        if (Enemy.health > 0)
        {
            AimOnTarget();
        }
            

            if (useLaser)
            {
                Laser();
            }
            else
            {
                fireCountDwn();
            }
        }
  
        void AimOnTarget()
    {
        //######Look on Target Code##########
        // direction to the  current target 
        Vector3 dir = target.position - transform.position;
        // Unitys way to deal with location, how we have to turn to look the target way
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        // smooth out the transition, from our current rotation to the new rotation between the states over Time
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        // Convert Quaternion (math way) to euler angles -> (unitys way) to deal angles
        // We make that euler convertion only to avoid rotating around all the axis x y z 
        // because we only need y axis, otherwise we could have used lookRotation. (Quaternion)
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);

    }

    void fireCountDwn()
    {
        // handle fire countdown
        if (fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 1f / fireRate;
        }

        fireCountdown -= Time.deltaTime;
    }

    void Laser()
    {
        Enemy.TakeDMG(damageOverTime * Time.deltaTime);
        Enemy.Slow(slowPercent);
        if (useLaser)
        {
            lineRenderer.enabled = true;
            impactEffect.Play();
            if (!shootSound.isPlaying)
            {
                shootSound.Play();
            }
            else
            {
                
            }
        }

        lineRenderer.SetPosition(0, firePoint.position);
        lineRenderer.SetPosition(1, target.position);

        Vector3 Turretdirection = firePoint.position - target.position;

        impactEffect.transform.position = target.position + Turretdirection.normalized * 0.5f;
        impactEffect.transform.rotation = Quaternion.LookRotation(Turretdirection);     
    }

    void Shoot()
    {
        GameObject bulletGO = (GameObject)( Instantiate(bulletPrefab, firePoint.position, firePoint.rotation));
        // Store bulletScript, find Component of that Bullet
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        if (!shootSound.isPlaying && isLoop == false)
        {
            shootSound.Play();
        }
        else
        {
            
        }
        if (bullet != null)
        { // is actuall a component on that object
            bullet.seek(target.GetChild(2));
        }
    }

    // unity callback function, to show the range of the turrets in the szene with Gizmos
    // delete Selected in method name to OnDrawGizmos(){::} draw always gizmos on transform position.
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{

	[SerializeField] private GameObject PauseUI;

    //Jump to the Main Menu
    public void OnMenu()
    {
	    SceneManager.LoadScene("MainMenu");
    }
    //Jump to Credits
    public void OnCredits()
    {
	    SceneManager.LoadScene("Credits");
    }
    //Exit Game
    public void OnExit()
    {
		#if UNITY_EDITOR
			    EditorApplication.isPlaying = false;
		#elif UNITY_WEBGL
				Application.OpenURL("about:blank");
		#else	
				Application.Quit(0);
		#endif
    }
    //Start Game
    public void OnStart()
    {
	    SceneManager.LoadScene("BraviaDayNight");
    }
    //Resume Game
    public void OnResume()
    {
	    PauseUI.SetActive(false);
	    Time.timeScale = 1;
    }
    //
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

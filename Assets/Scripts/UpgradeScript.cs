﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UpgradeScript {
    public GameObject upgradePrefab;
    public int upgradeCost;
    public int sellAmount;

    public List<GameObject> ArcherList = new List<GameObject>();
    public List<GameObject> CanonList = new List<GameObject>();
    public List<GameObject> MageList = new List<GameObject>();
    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

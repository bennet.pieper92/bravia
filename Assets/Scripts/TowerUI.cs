﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerUI : MonoBehaviour {

    private TowerPlattform plattform;
    public Vector3 possitionOffset;
    public GameObject UI;
    public GameObject upgradeUI;


    private void Awake()
    {
        possitionOffset.y = 100f;
        transform.position = plattform.getPosition() + possitionOffset;
    }

    private void Start()
    {
       
    }

    public void  setTarget (TowerPlattform _plattform)
    {
        plattform = _plattform;

        transform.position = plattform.getPosition() + possitionOffset;
        if (plattform.currIndex >= plattform.currBlueprint.maxIndex)
        {
            upgradeUI.SetActive(false);
        }
        else
        {
            upgradeUI.SetActive(true);            
        }
        UI.SetActive(true);
    }

    public void Hide()
    {
        UI.SetActive(false);
    }

    public void Upgrade()
    {
        plattform.UpgradeTower();
        BuildManager.instance.DeselectTower();
    }

    
    public void Sell()
    {
        plattform.SellTower();
        BuildManager.instance.DeselectTower();
    }

}

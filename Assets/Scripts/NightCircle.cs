﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightCircle : MonoBehaviour {

    public string moonTag = "Moon";
    public string sunTag = "Sun";
    public List<GameObject> crystalPrefabList;
   

	// Use this for initialization
	void Start () {
      
    }
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(Vector3.zero, Vector3.right, 2f * Time.deltaTime);
        transform.LookAt(Vector3.zero);

        if (transform.position.y >= -200)
        {
            foreach (GameObject go in crystalPrefabList)
            {
                go.SetActive(true);
            }
        }
        else
        {
            foreach (GameObject go in crystalPrefabList)
            {
                go.SetActive(false);
            }
        }
	}

    public static void switchLightOn()
    {
    
    }

  

    public static void switchLightOff()
    {

    }
}

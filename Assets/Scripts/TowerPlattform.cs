﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerPlattform : MonoBehaviour {

    public Color hoverColor = Color.blue;
    public Color ErrorHoverColor = Color.red;
    public Color notEnoughMoneyColor = Color.red;
    private Renderer rend;
    private Color startColor;
    private int upgradeCounter = 0;
    [Header("Upgrade")]
    public int currIndex;
    public TurretBlueprints currBlueprint;
    

    [HideInInspector]
    public GameObject tower;
    [HideInInspector]
    public TurretBlueprints towerBlueprints;
    //shop new
    [SerializeField] private Text upgradeText;
    [SerializeField] private Text sellText;
    public ShopNew shopNew;
    [HideInInspector]
    public bool isUpgraded = false;

    public Vector3 positionOffsetArcher01 = new Vector3(0, 5, 0);
    public Vector3 positionOffsetArcher02 = new Vector3(0, 10, 0);
    public Vector3 positionOffsetArcher03 = new Vector3(0, 10, 0);
    public Vector3 positionOffsetCanon01 = new Vector3(0, 10, 0);
    public Vector3 vector;

   BuildManager buildManger;



    private void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        buildManger = BuildManager.instance;
        upgradeCounter = 0;
    }

    private void Update()
    {
        // if (turret != null)
        // {
        //   Debug.Log("Can't build, already used place!");
        //    rend.material.color = ErrorHoverColor;
        // }
       
    }

    public void setOffset(Vector3 v)
    {
        this.vector = v;
    }
    public Vector3 getOffset()
    {
        return vector;
    }

    private void OnMouseDown()
    { 
        // blocking behavior, when we hover over event system object (shopItemns) return/exit function
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
       
        // there is allready a tower on the plattform
        if (tower != null)
        {
         
            rend.material.color = ErrorHoverColor;
            buildManger.SelectTower(this);
            return;
        }

        if (!buildManger.CanBuild())
        
            return;

        //buildManger.buildTurretOn(this); 
        BuildTurret(buildManger.GetTurretToBuild());
    }

    void BuildTurret (TurretBlueprints blueprint)
    {
        if (PlayerStats.money < blueprint.costs)
        {
            Debug.Log("Not enough money");
            return;
        }
        else
        {
            PlayerStats.money -= blueprint.costs;
            Debug.Log("Tower build Money Left: " + PlayerStats.money);
            GameObject _tower = Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.identity);
            // GameObject effect = (GameObject)Instatiate(buildManger.buildEffect, this.GetBuildPosition(), Quaternion.identity);
            // Destroy(effect, 5f);
            tower = _tower;
            towerBlueprints = blueprint;
            Debug.Log("Tower Build");
            currIndex = 0;
            upgradeText.text = "UPGRADE "+towerBlueprints.updateCostList[currIndex]+"$";
            sellText.text = "Sell "+towerBlueprints.sellAmountList[currIndex]+"$";
            currBlueprint = blueprint;
            
        }

    }

    public void UpgradeTower()
    {
        if (PlayerStats.money <= towerBlueprints.updateCostList[currIndex] && currIndex <= towerBlueprints.maxIndex)
        {
            Debug.Log("Not enough money");
            return;
        }
        else
        {
            PlayerStats.money -= towerBlueprints.updateCostList[currIndex];
            // Neu Offset in Upgrade
            Vector3 offsetUpgrade = new Vector3 (0, 10, 0);
            //Get rid of the old tower
            Destroy(tower);

            //Build a new tower with specific offset
            GameObject _tower = Instantiate(towerBlueprints.upgradeList[currIndex+1], getPosition() + offsetUpgrade, Quaternion.identity);
            upgradeText.text = "UPGRADE "+towerBlueprints.updateCostList[currIndex+1]+"$";
            sellText.text = "Sell "+towerBlueprints.sellAmountList[currIndex+1]+"$";
            tower = _tower;
            // Could be another effect for update rather then build effect
            // GameObject effect = (GameObject)Instatiate(buildManger.buildEffect, this.GetBuildPosition(), Quaternion.identity);
            // Destroy(effect, 5f);
            currIndex++;
                Debug.Log("Current Index is: " + currIndex);
                Debug.Log("Tower Upgraded!");
        }
    }

    // Neu mit Array und index, für jede Sorte ein Array?
   

    public void SellTower()
    {
        if (tower != null)
        {
            Destroy(tower);
            PlayerStats.money += towerBlueprints.sellAmountList[currIndex];
            Debug.Log("Tower sold");
            currIndex = 0;
            return;
        }
        else
        {
            Debug.Log("No Tower to sell");
        }
    }


    //callback like start, update, collision..
    private void OnMouseEnter()
    {
        if (tower != null)
        {
            Debug.Log("Can't build, already used place!");
            rend.material.color = ErrorHoverColor;
        }
        // blocking behavior, when we hover over event system object (shopItemns) return/exit function
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        // When there is no Turret to build exit
        /*if( BuildManager.instance.GetTurretToBuild() == null)
        {
            return;
        } */
        if (!buildManger.CanBuild())
        {
            return; 
        }
        if (buildManger.EnoughMoney() && tower == null)
        {
            rend.material.color = hoverColor;
        }
        else if (!buildManger.EnoughMoney())
        {
            rend.material.color = notEnoughMoneyColor;
        }
       
    }

    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }

    public Vector3 GetBuildPosition()
    {

        //TODO: check wich Turret is set and take the matching offset
        // if ( buildManger.GetTurretToBuild == buildManger.turretToBuildBlue.prefab ){}
        if ( buildManger.getTower() != null)
        {
            if (buildManger.getTower() == buildManger.ArcherTower01)
            {
                setOffset(positionOffsetArcher01);
            }
            /*   if (buildManger.getTower() == buildManger.ArcherTower02)
               {
                   setOffset(positionOffsetArcher02);
               }
               if (buildManger.getTower() == buildManger.ArcherTower03)
               {
                   setOffset(positionOffsetArcher03);
               }*/
            if (buildManger.getTower() == buildManger.CanonTower01)
            {
                setOffset(positionOffsetCanon01);
            }
            return transform.position + getOffset();
        }
        
        else {
            return transform.position;
        }
    }
    public Vector3 getPosition()
    {
        return this.transform.position;
    }

    public Vector3 getPositionUpgrade()
    {
        if (tower != null)
        {
            if (buildManger.getTower() == buildManger.ArcherTower01)
            {
                setOffset(positionOffsetArcher01);
            }
           /* if (buildManger.getTower() == buildManger.ArcherTower02)
            {
                setOffset(positionOffsetArcher02);
            }
           /* if (buildManger.getTower() == buildManger.ArcherTower03)
            {
                setOffset(positionOffsetArcher03);
            } */
            if (buildManger.getTower() == buildManger.CanonTower01)
            {
                setOffset(positionOffsetCanon01);
            }
        }
        return transform.position + getOffset();
    }

}

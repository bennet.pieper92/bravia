﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private Transform target;
    public float explosionRadius = 0f;
    public float speed = 70f;
    public int damage = 50;
    public static float score = 0f;
    public static Bullet bulletInstance;
    public GameObject impactEffect;

    [Header("Audio")] 
    public AudioSource bulletExplosion;
    public bool isPlayExplosion;
    public MeshRenderer bulletMesh;
    public bool dead;
  
    public float GetScore()
    {
        Debug.Log("Score" + score);
        return score;
    }
    public void IncrScore()
    {
        score ++;
    }
    // take a transform and setup everthing we need for bullet eq. dmg amount, speed, effects 
    public void seek(Transform Target)
    {
        target = Target;
    }
	
	// Update is called once per frame
	void Update () {
        if (dead)
        {
            return;
        }
		if(target == null)
        {
            Destroy(gameObject);
            return;
        }
        // direction from bullet to target (vector point in R^3)
        Vector3 dir = target.position - transform.position;
        // is the actuall distance we going to move the particular Frame
        float distanceThisFrame = speed * Time.deltaTime;
        // if the length of our direction from the bullet to the target
        // is less or equals to d
        if (dir.magnitude <= distanceThisFrame)
        {
            dead = true;
            HitTarget();
            return;
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);
	}

    void HitTarget()
    {
        // instantiate effect with current pos and rotation
        GameObject effectInstance = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectInstance, 2f);

        if (explosionRadius > 0f)
        {
            Explode();
        }
        else
        {
            Damage(target);
        }
        bulletMesh.enabled = false;
        Destroy(gameObject, 1f);
        IncrScore();

    }

    void Explode()
    {
        isPlayExplosion = false;
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                print("Hit multiple enemies");
                isPlayExplosion = true;
                Damage(collider.transform);
            }
        }

        if (isPlayExplosion)
        {
            bulletExplosion.Play();
        }
        else
        {
            
        }
    }

    void Damage (Transform enemy)
    {
        EnemyMovement enemyObj = enemy.GetComponent<EnemyMovement>();

        if (enemyObj != null)
        {
            enemyObj.TakeDMG(damage);
        }
    }

     void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}

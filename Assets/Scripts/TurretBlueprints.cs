﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurretBlueprints  {
   
    [Header("Normal")]
    public GameObject prefab;
    public int costs;
    public int sellAmount;
  
    [Header("Upgrade")]
    public List<GameObject> upgradeList;
    public List<int> sellAmountList;
    public List<int> updateCostList;
    public int maxIndex = 4;


    // could be a array or list of towers for multiple upgrades, follow just for one upgrade


}

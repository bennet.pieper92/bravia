﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
    public static int Rounds;
    public Text roundsText;


    // Use this for initialization
	void Start () {
        Rounds = 0;
	}
	
	// Update is called once per frame
	void Update () {

    }
    private void OnEnable()
    {
        roundsText.text = Rounds.ToString();

    }
    public void Retry()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene( SceneManager.GetActiveScene().name );
    }

    public void Menu()
    {
        SceneManager.LoadScene( "MainMenu" );
    }
}

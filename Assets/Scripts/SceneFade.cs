﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneFade : MonoBehaviour
{

    public Image image;
    public AnimationCurve curve;
    private void Start()
    {
      
    }

    public void FadeTo(string scene)
    {
        StartCoroutine(FadeIn(scene));
    }

    IEnumerator FadeIn(string sceneIn)
    {
        float t = 1f;

        while (t > 0f)
        {
            {
                t -= Time.deltaTime;
                float alpha = curve.Evaluate(t);
                image.color = new Color(0f, 0f, 0f, alpha);
                yield return new WaitForSeconds(0.05f);
            }
            SceneManager.LoadScene(sceneIn);
        }
    }

    IEnumerator FadeOut(string sceneOut)
        {
            
            float t = 0f;

            while (t < 1f)
            {
                {
                    t += Time.deltaTime;
                    float alpha = curve.Evaluate(t);
                    image.color = new Color(0f, 0f, 0f, alpha);
                    yield return new WaitForSeconds(0.05f);
                }
                SceneManager.LoadScene(sceneOut);
            }
        }
}

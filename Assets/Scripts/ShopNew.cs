﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Shop Script  
public class ShopNew : MonoBehaviour
{

    BuildManager buildManger;
    public TurretBlueprints ArchTower01;
    public TurretBlueprints CanonTower01;
    public TurretBlueprints MageTower01;

   
    
    private void Start()
    {
        buildManger = BuildManager.instance;
    }

    public void SelectArcherTower01()
    {
        Debug.Log("Archer Tower Level 1 Purchased");
        buildManger.selectTurretToBuild(ArchTower01);
    }

    public void SelectCanonTower01()
    {
        Debug.Log("Canon Tower Level 1 Purchased");
        buildManger.selectTurretToBuild(CanonTower01);
    }

    public void SelectMageTower01()
    {
        Debug.Log("Mage Tower Level 1 Purchased");
        buildManger.selectTurretToBuild(MageTower01);
    }

}



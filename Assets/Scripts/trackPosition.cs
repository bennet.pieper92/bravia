﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trackPosition : MonoBehaviour
{
	[SerializeField] private Transform targetTransform;
	[SerializeField] private Vector3 offsetY_axis;
	// Use this for initialization
	void Start () {
		offsetY_axis = new Vector3(0,55,0);
	}
	
	// Update is called once per frame
	void Update ()
	{
		setHealthbarPosition();
	}

	void setHealthbarPosition()
	{
		this.transform.position = targetTransform.position + offsetY_axis;
		this.transform.rotation = new Quaternion(40, 0, 0, 0);
	}
}

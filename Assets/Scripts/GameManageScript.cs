﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManageScript : MonoBehaviour
{

    public bool gameOver;
    public bool isPaused;
    public GameObject gameOverUI;
    public GameObject pauseMenu;
	
    // Use this for initialization
	void Start () {
        gameOver = false;
        isPaused = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (gameOver)
        {
            return;
        }
           

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseManager();
            if (isPaused)
            {
                Pause();
            }
            else if (!isPaused)
            {
                pauseMenu.SetActive(false);
                Time.timeScale = 1;
            }
        }
        if (PlayerStats.live <= 0)
        {
            GameOverFct();
        }
	}

    void GameOverFct ()
    {
        gameOver = true;
        gameOverUI.SetActive(true);
        Time.timeScale = 0;
    }

    void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        isPaused = false;
    }

    void PauseManager()
    {
        if (isPaused)
        {
            isPaused = false;
        }
        else
        {
            isPaused = true;
        }
    }
}

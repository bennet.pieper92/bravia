﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    Transform turretTransform;
    // Use this for initialization
    void Start () {
       turretTransform = transform.Find("turret01");
	}
	
	// Update is called once per frame
	void Update () {
        // TODO: optimze this!
        Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();

        Enemy nearestEnemy = null;
        float distance = Mathf.Infinity;

        foreach(Enemy e in enemies)
        {
            //Debug.Log("enemy: " + e);
           // Debug.Log("enemies: " + enemies);
            Debug.Log("Enemys Array Length = " + enemies.Length);
            float d = Vector3.Distance(this.transform.position, e.transform.position);
            if(nearestEnemy == null || d < distance)
            {
                nearestEnemy = e;
                distance = d;

            }
        }

        if( nearestEnemy == null)
        {
            Debug.Log("No enemie?");
            return;
        }

        Vector2 dir = nearestEnemy.transform.position - this.transform.position;

        Quaternion lookRotation = Quaternion.LookRotation(dir);


        Debug.Log("Look Rotation angles = " + lookRotation.eulerAngles);
         //Debug.Log("Look Rotation y = " + lookRotation.eulerAngles.y);
      //  Debug.Log("Look Rotation z = " + lookRotation.eulerAngles.y);
        turretTransform.rotation = Quaternion.Euler(-90, 0 , lookRotation.x); // geht nicht
	}
}

﻿using UnityEngine;

public class BuildManager : MonoBehaviour {

    public static BuildManager instance;
    public GameObject ArcherTower01;
    public GameObject CanonTower01;
    public GameObject MageTower01;
    public GameObject ArcherTowerUpgrade;
    private GameObject turretToBuild;
    private TurretBlueprints turretToBuildBlue;
    private TowerPlattform selectedTower;
    public TowerUI towerUI;

    // called before start, every Time we start the Game we store this BuildManager in our instance
    private void Awake()
    {
        if(instance != null)
        {
            Debug.Log("There is already a BuildManger in Scene!");
        }
        instance = this;
    }

    private void Start()
    {
        turretToBuild = ArcherTower01;
    }

   /* public GameObject GetTurretToBuild()
    {
        return turretToBuild;
    } 

    public void SetTurretToBuild(GameObject turret)
    {
        turretToBuild = turret;
    } */

    public bool CanBuild ()
    {
        if( turretToBuildBlue == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool EnoughMoney()
    {
        if (PlayerStats.money >= turretToBuildBlue.costs)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public TurretBlueprints TurretToBuild
    {
        get
        {
            return turretToBuildBlue;
        }
    }

    public void SelectTower ( TowerPlattform tower)
    {
        if (selectedTower == tower)
        {
            DeselectTower();
            return;
        }
        selectedTower = tower;
        turretToBuildBlue = null;

        towerUI.setTarget(tower);
    }

    public void DeselectTower()
    {
        selectedTower = null;
        towerUI.Hide();
    }

    public void selectTurretToBuild(TurretBlueprints turretBlueprint)
    {
        this.turretToBuildBlue = turretBlueprint;
        DeselectTower(); 
    }

    public TurretBlueprints GetTurretToBuild()
    {
        return turretToBuildBlue;
    }

    public GameObject getTower()
    {
        GameObject turret = turretToBuildBlue.prefab;
        return turret;
    } 

}

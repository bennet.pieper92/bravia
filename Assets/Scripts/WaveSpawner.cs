﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{

    public static int EnemiesAlive = 0;
    public Wave[] waves;
    public Transform enemyPrefab;
    public Transform spawnPoint;
    public float TimeBetweenWaves = 5f;
    private float countdown = 2f;
    public Text waveCountdownText;
    public Text waveNumberText;
    private int waveIndex = 0;
    [SerializeField] private Animator animatorCountdown;

    void Update()
    {
        if (EnemiesAlive > 0)
        {
            return;
        }
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            waveCountdownText.enabled = false;
            animatorCountdown.SetBool("isTimer", false);
            countdown = TimeBetweenWaves;
            return;
        }
        // reduce countdown with Time every Second again with Framerate depends on Machine Framerate
        countdown -= Time.deltaTime;
        waveCountdownText.enabled = true;
        animatorCountdown.SetBool("isTimer", true);
        // avoid negative numbers
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        // decimal format for Countdown
        waveCountdownText.text = string.Format("TIME "+ "{0:00.00}", countdown);
        waveNumberText.text = "WAVE: " + waveIndex.ToString();
    }
    
    IEnumerator SpawnWave()
    {
        
        GameOver.Rounds++;

        Wave wave = waves[waveIndex];  
        for (int i = 0; i < wave.enemyAmount; i++)
        {
            SpawnEnemy(wave.enemyPrefab);
            yield return new WaitForSeconds(1f / wave.spawnRate); // waite between one enemy to spawn with rate
        }
        waveIndex++;
        if (waveIndex == waves.Length)
        {
            print("Level Won");
            this.enabled = false; // disable script when all waves done
            //SceneManager.LoadScene("Winning");
            // In future load next Level here!
        }
        
    }

    void SpawnEnemy(GameObject enemy)
    {
        //spawning enemyPrefab at SpawnPoint position und SpawnPoint rotation
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        EnemiesAlive++;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float panSpeed = 200f;
    private float panBoarderThikness = 10;
    public float scroolSpeed = 20f;
    // destrict the scrolling
    public float minY = 200;
    public float maxY = 300;
    public float minX = -100;
    public float maxX = 650;
    public float minZ = -500;
    public float maxZ = 450;
    public GameManageScript manager;

    public GameObject pauseUI;
    //controll movement
    public bool isMove = true;
    
    // Use this for initialization
    void Start() {
     

    }
	// Update is called once per frame
	void Update () {

        // stop the movement
        if (!pauseUI.active)
        {
            isMove = !isMove;
        }
        if (pauseUI.active)
        {
            return;
        }

        // when game over disable camera controlls
        if (manager.gameOver)
        {
            this.enabled = false;
            return;
        }
        

        if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - panBoarderThikness)
        {
            transform.Translate(Vector3.forward * panSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey("s") || Input.mousePosition.y <= panBoarderThikness)
        {
            transform.Translate(Vector3.back * panSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - panBoarderThikness)
        {
            transform.Translate(Vector3.right * panSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey("a") || Input.mousePosition.x <= panBoarderThikness)
        {
            transform.Translate(Vector3.left * panSpeed * Time.deltaTime, Space.World);
        }
        
        
        // Scroll handling
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        // our current pos vector
        Vector3 pos = transform.position;
        // subtract from that our scroll mult with the speed
        pos.y -= scroll * 1000 * scroolSpeed * Time.deltaTime;
        pos.y = Mathf.Clamp(pos.y, minY, maxY);
        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.z = Mathf.Clamp(pos.z, minZ, maxZ);
        // Set the Transform position equals the new
        transform.position = pos;

    }
    }


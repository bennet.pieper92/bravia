﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public static MusicManager instance;
    private bool isMusicPlaying;
    public AudioSource audioSource; // Declare Variable Step #1
   

    private void Awake()
    {
        //MakeSingLeton();
       // audioSource = GetComponent<AudioSource>();  // Step #2 Assign Value, could be to in UnityEditor managed
    }

   /*  void MakeSingLeton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    */

    public void StartStopMusic()
    {
        if (isMusicPlaying)
        {
            isMusicPlaying = false;
            audioSource.Stop();
        }
        else
        {
            isMusicPlaying = true;
            audioSource.Play(); // Use it Step #3
        }
          


    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

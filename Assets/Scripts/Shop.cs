﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Shop Script  
public class Shop : MonoBehaviour {

    BuildManager buildManger;
    public TurretBlueprints ArchTower01;
    public TurretBlueprints ArchTower02;
    public TurretBlueprints ArchTower03;


    private void Start()
    {
        buildManger = BuildManager.instance;
    }

    public void PurchaseArchTower01 ()
    {
        Debug.Log("Arch Tower Level 1 Purchased");
        //buildManger.SetTurretToBuild(buildManger.ArcherTower01);
    }

    public void PurchaseArchTower02()
    {
        Debug.Log("Arch Tower Level 2 Purchased");
        //buildManger.setTurretToBuild(buildManger.ArcherTower02);
    }

    public void PurchaseArchTower03()
    {
        Debug.Log("Arch Tower Level 3 Purchased");
       // buildManger.SetTurretToBuild(buildManger.ArcherTower03);
    }


}

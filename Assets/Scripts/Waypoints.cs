﻿using UnityEngine;

public class Waypoints : MonoBehaviour {

    // whenever we want a list of Gameobjects in our Scene
    //Erstelle eine Liste von Gameobjects
    public static Transform[] waypoints;


    // Awake wird noch vor Start aufgerufen um Objekte einen Ticken früher zu initialisieren.
    // findet alle Child Objekte (alle Waypoints) des Parrent Object Waypoints.
    //
    private void Awake()
    {
        // initialisiere Array mit der groesse gleich Anzahl der Child (Waypoints)
        waypoints = new Transform[transform.childCount];
        //iteriere ueber array und füge an stelle i das passende Childobjekt hinzu
        for ( int i = 0; i < waypoints.Length; i++)
        {
            waypoints[i] = transform.GetChild(i);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    GameObject pathGO;
    Transform targetPathNode;
    int pathNodeIndex = 0;
    float speed = 50f;
    public int health = 1;

    // Use this for initialization
    void Start()
    {
        //Find a object named "Path"
        pathGO = GameObject.Find("Path");
    }

    void getNextPathNode()
    {
        targetPathNode = pathGO.transform.GetChild(pathNodeIndex);
        pathNodeIndex++;
    }

    // Update is called once per frame
    void Update()
    {
        if (targetPathNode == null)
        {
            getNextPathNode();
            if (targetPathNode == null)
            {
                // no path nodes left!
                ReachedGoal();
            }
        }
        // Direction to our target
        Vector3 direction = targetPathNode.position - this.transform.localPosition;

        float distanceThisFrame = speed * Time.deltaTime;

        if (direction.magnitude <= distanceThisFrame)
        {
            // We reached the node 
            targetPathNode = null;
        }
        else
        {
            //More Path Points to smooth out
            // Move towards the node position (Space.World /Translate do it otherwise by local by default)
            transform.Translate(direction.normalized * distanceThisFrame, Space.World);
            // rotation 
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 5);


        }
    }
    void ReachedGoal()
    {
        Destroy(gameObject);
    }
}
